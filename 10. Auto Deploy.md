# Auto Deploy

> **NOTE:** If you are using the GitLab Demo system for this workshop, then skip this lab. Your assigned Demo system account does not have permissions to manage the pre-provisioned K8s cluster you have been using in previous labs. If you'd like to do this lab then you will need to use your own GitLab account and your AWS account credentials.

> **TO OBTAIN CLOUD PROVIDER CREDENTIALS:** For GitLab team members, use the [Sandbox Cloud](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#how-to-get-started). For non-GitLab members, please obtain cloud credentials via your own organizations.

[Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-deploy) is a stage used by [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/). Auto Deploy, as its names describes, automates the deployment of your application and it is also based on best practices. Auto Deploy supports deployment to Kubernetes (K8s), Amazon Elastic Container Service (ECS), and Amazon Elastic Compute Cloud (EC2).

## When to use Auto Deploy

These are some situations when to consider using Auto Deploy:

- If you plan to deploy and run your application on K8s. Auto Deploy for K8s is our most mature Auto Deploy and most integrated to the other Auto DevOps templates.
- If you plan to deploy to ECS, you should consider leveraging the Auto Deploy template for ECS. The use of this template requires pre-setup work to occur on ECS first, however.
- If you plan to deploy to EC2, you should consider leveraging the Auto Deploy template for EC2. The use of this template requires pre-setup work to occur on EC2 first, however.

## Auto Deploy for K8s

The requirements to Auto Deploy for K8s are very similar to the ones for running Auto DevOps on K8s:

1. A Kubernetes 1.12+ cluster for your project
2. NGINX Ingress
3. Base domain
4. GitLab Runner
5. Prometheus (optional but nice to have if you'd like to leverage the Auto Monitoring capabilities of GitLab with your deployment)
6. cert-manager (optional, for TLS/HTTPS)

For more information, go [here](https://docs.gitlab.com/ee/topics/autodevops/requirements.html#auto-devops-requirements-for-kubernetes).

### Helm and Tiller

Auto DevOps and Auto Deploy rely on [Helm](https://helm.sh/) to instantiate applications on K8s. In simple terms, Helm is a package manager for K8s, equivalent to yum on Linux. In Helm lingo, a K8s package is called a *chart*. The *chart* is a bundle of information necessary to create an instance of a Kubernetes application. The *config* contains configuration information that can be merged into a packaged chart to create a releasable object. A *release* is a running instance of a chart, combined with a specific config.

Helm has two main components:

1. **The Helm Client** is a command-line client for end users. The client is responsible for local chart development, managing repositories (charts can be stored in helm repositories), interacting with the Tiller server.
2. **The Tiller Server** is an in-cluster server that interacts with the Helm client, and interfaces with the Kubernetes API server. The server is responsible for listening for incoming requests from the Helm client, combining a chart and configuration to build a release, installing charts into Kubernetes and then tracking the subsequent release, upgrading and uninstalling charts by interacting with Kubernetes.

In short, the client is responsible for managing charts, and the server is responsible for managing releases. For more information, please go [here](https://v2.helm.sh/docs/).

GitLab offers integrated cluster creation for Google Kubernetes Engine (GKE) and Amazon Elastic Kubernetes Service (EKS). When creating a K8s cluster, GitLab automates the installation of a Tiller server onto the Kubernetes cluster via the *Applications* tab in the Kubernetes Clusters window. We will cover this in the next section.

Lastly, GitLab provides its own Helm chart [repository](https://charts.gitlab.io/) where all of GitLab's official helm charts live. In there, you will find charts used for installing: GitLab, GitLab Runner, Knative, Elastic Stack, etc.

### Setting up a K8s cluster on GitLab.com for Auto DevOps and Auto Deploy

In this lab, we will go over the instructions on how to use the GitLab integrated cluster creation capabilities to create an EKS cluster. 

For the creation of other clusters, please refer to the following resources:

- How to create an GKE cluster [documentation](https://docs.gitlab.com/ee/user/project/clusters/add_gke_clusters.html)
- How to deploy to an Azure Kubernetes Service (AKS) [blogpost](https://medium.com/@jonathanjfshaw/connecting-gitlab-to-your-azure-kubernetes-cluster-cefcaeb54c1f)
- [Using GitLab with OpenShift](subchapters/using-GitLab-with-OpenShift.md)

### Setting up an EKS cluster

GitLab offers integrated cluster creation for EKS, which makes this task very easy.

Using your own GitLab account and your AWS account credentials, follow the step-by-step [instructions](subchapters/create-eks-cluster.md) to create an EKS cluster from GitLab.

There are two other sources that you can use to create the EKS cluster:

1. [Adding EKS Clusters GitLab documentation](https://docs.gitlab.com/ee/user/project/clusters/add_eks_clusters.html)
2. [How to deploy your application to a GitLab-managed Amazon EKS cluster with Auto DevOps - blogpost](https://about.gitlab.com/blog/2020/05/05/deploying-application-eks/)

### Using Auto Deploy for K8s

Auto Deploy is a stage used by Auto DevOps. One of the deployment platforms that Auto Deploy supports is K8s. The Auto Deploy template for K8s is based on best practices and it is well integrated with the other stages of Auto DevOps. GitLab provides configurable options to leverage Auto Deploy via the enablement of Auto DevOps. We covered how to enable Auto DevOps in the lab *[Enabling Auto DevOps](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/01.%20Auto%20DevOps.md)*, in which we briefly discussed the Auto Deploy template. We also covered how to leverage the Auto Deploy template on its own as well as how to use parts of Auto DevOps in the lab *[Leveraging portions of Auto DevOps in your pipeline](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/03.%20Auto%20DevOps%20Customization.md#leveraging-portions-of-auto-devops-in-your-pipeline)*. In this lab, let's delve deeper into the three Auto Deploy configurable options through Auto DevOps.

You should have a sample project (with an application in it) and a project-level EKS cluster up and running. Let's deploy the application to the cluster. As we do this, we will explore the different Auto Deploy configurable options that GitLab provides.

Head over to **Settings > CI/CD** and then scroll down to the section titled **Auto DevOps**. Click on the *Expand* button next to it to see its details. Go ahead and click on the checkbox **Default to Auto DevOps pipeline**:

<img src="images/auto-deploy/46-auto-devops-continuous-deploy.png" width="75%" height="75%">

In the screenshot above, you can see the three deployment strategies that the Auto Deploy template offers. They are:

> **NOTE:** the official [Gitlab Auto Deploy template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml) also offers staging, canary, incremental and timed-incremental deployment strategies. They can be enabled via project variables.

- Continuous deployment to production
- Continuous deployment to production using timed incremental rollout
- Automatic deployment to staging, manual deployment to production

Go ahead and select the radio button for *Continuous deployment to production* and then click on the *Save changes* green button. The save will trigger a pipeline to start. Go to **CI/CD > Pipelines** and click on the number of the running pipeline:

<img src="images/auto-deploy/47-continuous-deploy-pipeline.png" width="75%" height="75%">

This will take you to the detail view of the pipeline:

<img src="images/auto-deploy/48-continuous-deploy-detail.png" width="90%" height="90%">

In the detail view of the pipeline above, you can see that after all tests are done, the pipeline deploys the application directly to production (there are no intermediate staging or canary deployment stages before production). This is called **continuous deployment**.

You can cancel/stop the running pipeline at this point (you do this from the **CI/CD > Pipelines** screen), if you'd like.

Let's check out the second deployment strategy now. Head over to **Settings > CI/CD** and then scroll down to the section titled **Auto DevOps**. Click on the *Expand* button next to it to see its details. This time, select the seccond radio button *Continuous deployment to production using timed incremental rollout* and then click on the *Save changes* green button:

<img src="images/auto-deploy/49-auto-devops-time-incremental.png" width="75%" height="75%">

The save will trigger a pipeline to start.

> **NOTE:** if a pipeline is not automatically triggered by the save, go to **CI/CD > Pipelines** and then click on the *Run Pipeline* green button on the top right of the screen. This will take you to the **Run pipeline** screen. On this screen, make sure that the **Run for branch name or tag** is set to *master*, before clicking on the *Run Pipeline* button at the bottom of the screen.

Like for the previous deployment, go back to its detailed pipeline view and you will see the following pipeline:

<img src="images/auto-deploy/50-timed-incremental-pipeline.png" width="100%" height="100%">

In the detail view of the pipeline above, you can see that after all tests are done, the pipeline deploys the application directly to production using a timed rollout from 10% to 25%, then to 50%, and finally to all of production (100%). The interval between these rollouts is 1 minute. This provides the opportunity to monitor and observe the behavior of the new rollout in real time as its rollout starts to fill up all the production pods. More importantly, if any issues are encountered during these step-up timed rollouts, you can cancel the rollout and perform a manual rollback to restore production to its last working state during the 1-minute intervals. This reduces risk of downtimes and keeps user experience at an optimal level.

> **NOTE:** We covered timed-rollout in detail in the lab titled *[Incremental rollouts](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/06.%20Incremental%20Rollouts.md#timed-rollout-in-action)*.

You can cancel/stop the running pipeline at this point (you do this from the **CI/CD > Pipelines** screen), if you'd like.

Let's check out the third deployment strategy now. Head over to **Settings > CI/CD** and then scroll down to the section titled **Auto DevOps**. Click on the *Expand* button next to it to see its details. This time, select the seccond radio button *Automatic deployment to staging, manual deployment to production* and then click on the *Save changes* green button:

<img src="images/auto-deploy/51-auto-devops-manual-deploy.png" width="75%" height="75%">

The save will trigger a pipeline to start.

> **NOTE:** if a pipeline is not automatically triggered by the save, go to **CI/CD > Pipelines** and then click on the *Run Pipeline* green button on the top right of the screen. This will take you to the **Run pipeline** screen. On this screen, make sure that the **Run for branch name or tag** is set to *master*, before clicking on the *Run Pipeline* button at the bottom of the screen.

Like for the previous deployment, go back to its detailed pipeline view and you will see the following pipeline:

<img src="images/auto-deploy/52-manual-deploy-pipeline.png" width="90%" height="90%">

In the detail view of the pipeline above, you can see that after all tests are done, the pipeline first deploys the application to the staging environment. While the application is running in staging, you have the opportunity to check it and validate it. You can then manually roll out the application to production using a manual incremental rollout from 10% to 25%, then to 50%, and finally to all of production (100%). This provides the opportunity to monitor and observe the behavior of the new rollout in real time as you roll it out to the production pods. More importantly, if any issues are encountered during these step-up manual incremental rollouts, you can cancel the rollout and perform a manual rollback to restore production to its last working state. This reduces risk of downtimes and keeps user experience at an optimal level.

> **NOTE:** We covered timed-rollout in detail in the lab titled *[Incremental rollouts](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/06.%20Incremental%20Rollouts.md#incremental-rollout-in-action)*.

If you'd like to check the running application in the production or staging environment, you can go to the Environments page by clicking on **Operations > Environments**:

<img src="images/auto-deploy/53-running-envs.png" width="75%" height="75%">

From the screen above, you can click on the *Open live environment* button <img src="images/auto-deploy/60-open-live-env-button.png" width="3%" height="3%"> on the right hand side of the environment entry to open a new tab on your browser with the running application.

### Troubleshooting

You have a few options for troubleshooting errors that you may encounter during the deployment of your applications to K8s. A list of some of these options follows:

1. **The specific auto deploy job log output**. On the graphical view of a pipeline run (from the **CI/CD > Pipelines** window, click on a specific pipeline), you can click on a job node. This will take you to the log output of that job. In it, you will see any output from errors that occurred during the executing of the specific job in the pipeline. The error message itself may give you insight into the cause of the error. Here's an example screenshot:

<img src="images/auto-deploy/59-job-log.png" width="75%" height="75%">

On the picture above, you can see the log output for the manual deploy job *rollout 100%*, which deploys the application to all of production under the **Production** stage. Although, this log does not show any errors, this is where you would see them in case any happen.

2. **The Environments Terminal window**. From the **Operations > Environments** window, you can click on the *Terminal* button <img src="images/auto-deploy/61-terminal-button.png" width="3%" height="3%"> on the right hand side of the environment entry to open a Terminal window for your environment.

<img src="images/auto-deploy/58-click-on-terminal.png" width="75%" height="75%">

Once you click on the Terminal button, you will be taken to a Terminal window that is already authenticated and logged on to the container for the environment you selected. Here's an example of a Terminal window for the production environment:

<img src="images/auto-deploy/62-prod-terminal-window.png" width="50%" height="50%">

From the Terminal window above, you can enter commands on the production server. For example, you can see the two commands, *ls -l*, and *ls /*, the user entered and the corresponding output for each.

3. **The command line in your desktop/laptop**. To connect to your EKS K8s cluster from your local desktop/laptop, you will first need to do some setup as follows:

> **NOTE:** we are assuming that you are running a Unix-flavored operating system for some of the steps below. You will need to translaste these to Windows if that's what you're using.

- Install the AWS CLI version 2, which is the most recent major version of the AWS CLI and supports all of the latest features. To install it on your desktop/laptop, please follow the instructions found [here](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html).
- Ensure that the public and private keys in your $HOME/.ssh directory in your desktop/laptop match the keys you used when you created your key pair in step 5 of lab *[Creating AWS policy and roles](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/create-aws-roles-for-eks.md)*.
- Create a kubeconfig entry for your EKS cluster. This process will create the $HOME/.kube directory and insert an entry for your EKS cluster in its config sub-directory. To accomplish this, please follow the instructions [here](https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html). Adding this entry to your .kube/config file is convenient so that you don't have to specify the region, cluster name or role-arn with every kubectl command.

Once you have set up all the components to be able to connect to your EKS cluster from your desktop/laptop, open a Terminal window and use kubectl commands to access your EKS cluster without having to specify the cluster name, provisioning role ARN, and region every time you enter a kubectl command.

Here are some useful commands for troubleshooting:

- To get your AWS user identity from your local command line (who are you logged on as to AWS?):

> aws sts get-caller-identity

- To add a newly created EKS to your local kubernetes configuration (creates a kubeconfig entry for your EKS cluster):

> aws --region [REGION] eks update-kubeconfig --name [EKS CLUSTER NAME] --role-arn [PROVISIONING ROLE ARN]

- To see status of all pods under all namespaces (the *--watch* is for continuously streaming the end of the file, similar to *tail -f*):

> kubectl get pods --all-namespaces —-watch

- To see events on a cluster under a specific namespace, sorted by time (**very informative command**):

> kubectl -n [K8s NAMESPACE]  get events --sort-by='{.lastTimestamp}'

- To see details of a specific pod in a specific namespace:

> kubectl describe pods [pod NAME] -n [K8s NAMESPACE]

- To see the logs of a pod in a specifi namespace (the *-f* is for continuously streaming the end of the file, similar to *tail -f*):

> kubectl logs -f [pod NAME] -n [K8s NAMESPACE]

- To see the logs (with -f option) for a specific pod of a specific container of a specific namespace

> kubectl logs -f [pod NAME] -c [CONTAINER NAME - get it with describe option above] -n [K8s NAMESPACE]

### Building locally with Docker before deploying to GitLab.com

When developing a microservice, you may want to implement a first version on your local machine. In other words, in situations when there's no need for collaborating with others to get an initial version of a microservice to build and run, you will want to get the microservice to work locally first before you start using a pipeline. Building your application using pipelines can be very time consuming so you may want to get your microservice to work locally first before you start using pipelines to build it.

If you application uses Docker then you can get your microservice to build and run locally first. Here are some useful commands that can help you do this:

- If your application has a Dockerfile, then change directory to where its Dockerfile is and run:

> docker build -t [IMAGE TAG NAME] .

- To see docker images in your local docker repository:

> docker images

- If you'd like to run the latest Docker image of your application on a specific port

> docker run -p [HOST PORT NUMBER]:[CONTAINER PORT NUMBER] [IMAGE TAG NAME]:latest

- To see running docker images (enter it from another terminal window):

> docker ps

Now that we have covered Auto Deploy to K8s, specifically EKS, let's move on to the next topic, Amazon ECS.

## Auto Deploy for ECS

The requirements to Auto Deploy for ECS are:

1. Create an AWS Task Definition defined
2. Create an ECS cluster created
3. Create an Application Load Balancer for your ECS cluster
3. Create a cluster Service
4. Adjust Security Group of EC2 instance of ECS cluster

For more information, go [here](https://docs.gitlab.com/ee/topics/autodevops/requirements.html#auto-devops-requirements-for-amazon-ecs).

### Setting up ECS for Auto Deploy

### Using Auto Deploy for ECS

<!--
## Auto Deploy for EC2

For more information, go [here](https://docs.gitlab.com/ee/topics/autodevops/requirements.html#auto-devops-requirements-for-amazon-ec2)

### Setting up EC2 for Auto Deploy

### Using Auto Deploy for EC2

-->
