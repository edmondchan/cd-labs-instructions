## Creating a project called my-sample-project-1

In this lab, you will go through steps to create a project called *my-sample-project-1*.

1. Log on to your GitLab workspace.

2. **Creating a group** (skip if you already created this group)

This project must reside inside a group so you must first create one. Select *Create group* from the **Groups** menu:

<img src="images/demo-system/19-click-on-create-group.png" width="30%" height="30%">

In the **New group** window, enter *My Test Group* in the **Group name** field:

<img src="images/demo-system/20-create-group.png" width="50%" height="50%">

Click on the *Create group* blue button at the bottom of your screen. The group will be created and you will be put in it:

<img src="images/demo-system/21-group-created.png" width="50%" height="50%">

3. **Creating a project**

Navigate to the group *My Test Group* that you created in step 2 above.

Click on the *New project* blue button. On the **Create new project** window, click on the **Create from template** tile:

<img src="images/release/3.2CreateFromTemplate.png" width="50%" height="50%">

On the **Create from template** windows, you will see a list of **Built-in** templates. Scroll all the way to the bottom of the list until you see the entry for the template **Sample GitLab Project**:

<img src="images/release/3.3SampleProject.png" width="75%" height="75%">  

Click on the *Use template* green button for the template above. Once you have selected the project template, you will be prompted to give it a name, let's name it *sample-project-1*:

<img src="images/demo-system/22-create-proj.png" width="50%" height="50%">

Lastly, click on the *Create project* blue button at the bottom of the screen. You will see an *Importing in progress* message temporarily on your screen:

<img src="images/demo-system/17-proj-creation-in-progress.png" width="50%" height="50%">

Once the project is created, you will see the following:

<img src="images/demo-system/18-sample-proj-1-created.png" width="50%" height="50%">

Click [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/09.%20Releases.md#creating-a-project) to continue with this lab.
