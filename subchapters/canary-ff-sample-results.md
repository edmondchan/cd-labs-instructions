## Results of feature flag in a 4-pod production deployment with one canary pod

Following the step-by-step process from this chapter, we logged on as each valid user below in sequential order:

| Username | Password 
| ----------- | ----------- |
| pluto@gmail.com | pa33w0rd
| magic@cfl.rr.com | pa33w0rd
| mickey@disney.com | pa33w0rd
| hulk@universal.com | pa33w0rd
| minnie@disney.com | pa33w0rd
| thor@marvel.com | pa33w0rd

Here are the screenshots with the results:

1. When we logged on as *pluto@gmail.com*, we hit the canary pod (the updated application with the orange background) and we got the feature flag (products ordered in alphabetical order):

<img src="images/fflag/62-pluto-canary-ff.png" width="50%" height="50%">

2. When we logged on as *magic@cfl.rr.com*, we hit the old application (with the purple background) and we got the feature flag (products ordered in alphabetical order):

<img src="images/fflag/63-magic-nocanary-ff.png" width="50%" height="50%">

3. When we logged on as *mickey@disney.com*, we hit the old application (with the purple background) and we did not get the feature flag (products ordered by product ID):

<img src="images/fflag/64-mickey-nocanary-noff.png" width="50%" height="50%">

4. When we logged on as *hulk@universal.com*, we hit the old application (with the purple background) and we did not get the feature flag (products ordered by product ID):

<img src="images/fflag/65-hulk-nocanary-noff.png" width="50%" height="50%">

5. When we logged on as *minnie@disney.com*, we hit the old application (with the purple background) and we got the feature flag (products ordered in alphabetical order):

<img src="images/fflag/66-minnie-nocanary-ff.png" width="50%" height="50%">

6. When we logged on as *thor@marvel.com*, we hit the canary pod (the updated application with the orange background) and we did not get the feature flag (products ordered by product ID):

<img src="images/fflag/67-thor-canary-noff.png" width="50%" height="50%">

Notice that the the pods are accessed in sequential order as the web app requests come in. Looking at the pods in production:

<img src="images/fflag/59-prod-with-canary.png" width="50%" height="50%">

The first listed pod above is the canary pod with the updated application (orange background) followed by 4 pods with the old application (purple background). In the results above, *pluto@gmail.com* hit the first pod, which is the canary. The next 4 users, *magic@cfl.rr.com*, *mickey@disney.com*, *hulk@universal.com*, and *minnie@disney.com*, hit the next consecutive four non-canary pods, and *thor@marvel.com* hit the first canary pod again. In other words, incoming traffic is being served by the ingress in sequential order across all the pods in production. Depending on which pod is serving your first web app invocation, you may end up hitting the canary pod once or twice when trying logging in and out as each of the 6 users above.

Finally, notice that out of the six users we tried, 3 of them got the feature flag and 3 did not, which matches the 50% feature flag strategy for production. Some users got both, the feature and the new application, and some users got either one or the other. All these permutations are done for you by GitLab. However, wouldn't it be great to have more controlled over how the canary is deployed to production? This is where incremental rollouts can help.
