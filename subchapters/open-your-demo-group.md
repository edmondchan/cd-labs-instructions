# Opening your demo group

> **NOTE:** Follow these instructions if you're using the GitLab Demo System. Otherwise, skip.

Throughout the workshop, there will be labs that ask you to import projects. For the project templates to be listed in your importable list of projects in your GitLab workspace provided by the GitLab Demo System, you must be in the group that has been allocated to your assigned Demo System username.

There are two ways to open your Demo System assigned group listed below.

## From the screen where you redeemed your invitation code

When you [redeemed your GitLab Demo System invitation code](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/redeem-invitation-code.md), we asked you to keep the browser tab open because it contained the *My Group* button. Here's the screen of the browser tab we asked you to keep open:

<img src="images/demo-system/3-your-credentials.png" width="40%" height="40%">

To get to your demo group, click on the *My Group* blue button above. You can hold the CTRL or Command key as you click the button so that your group will be open in a new browser tab.

## By navigating to it once you are logged in

If you closed the browser tab from which you [redeemed your GitLab Demo System invitation code](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/redeem-invitation-code.md) or you cannot reopen it, then here's how to get to your demo group:

1. Open a browser window and enter the following URL in its address:

> https://gitlab-core.us.gitlabdemo.cloud

2. You will see the Demo Cloud login prompt:

<img src="images/demo-system/4-demo-cloud-login-prompt.png" width="50%" height="50%">

Enter your redeemed username and password in the fields above and click on the *Sign In* button.

3. You will be logged in and be place on your Projects window. To get to your designated group, click on the *Groups* menu, which will show up a pop-down menu that includes a search field. In the search field, start entering your assigned Demo System username. Your assigned group will appear right below the search field:

<img src="images/demo-system/5-explore-groups.png" width="40%" height="40%">

Click on your group, which will start with the prefix **My Test Group - [your Demo System username]**. In the screenshot above, my username was *iujyhojt* (yours will be different).

## Your demo group

Following either of the two methods mentioned above, at this point, you will be at your designated Demo System group, from which you can create sub-groups and/or import projects. Your screen should look similar to:

<img src="images/demo-system/6-your-group.png" width="50%" height="50%">

### Getting back to your demo group using the breadcrumb

During the workshop, you will be asked to create projects in your demo group. When you are inside a project, you can always use the breadcrumb located at the top of your screen to get back to your demo group level. For example, in the picture below, you can see the screen for project **simple-java** and at the top of it the breadcrumb *Training Users > ... > My Test Group - iujyhojt > simple-java* is displayed:

<img src="images/demo-system/16-breadcrumb.png" width="50%" height="50%">

If you click on the element *My Test Group - iujyhojt* of the breadcrumb, you will be taken to your demo group screen.
