## Creating a project called my-sample-project-1

In this lab, you will go through steps to create a project called *my-sample-project-1*.

1. **Creating a project**

Log on to your assigned GitLab Demo System workspace and head over to your demo group by following these [instructions](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/open-your-demo-group.md) (when done, click the *Back* button on your browser to return to this page).

From your demo group:

<img src="images/demo-system/6-your-group.png" width="50%" height="50%">

Click on the *New project* green button. On the **Create new project** window, click on the **Create from template** tile:

<img src="images/release/3.2CreateFromTemplate.png" width="50%" height="50%">

On the **Create from template** windows, you will see a list of **Built-in** templates. Scroll all the way to the bottom of the list until you see the entry for the template **Sample GitLab Project**:

<img src="images/release/3.3SampleProject.png" width="75%" height="75%">  

Click on the *Use template* green button for the template above. Once you have selected the project template, you will be prompted to give it a name, let's name it *sample-project-1*:

<img src="images/release/3.4SampleProjectCreation.png" width="75%" height="75%"> 

Lastly, click on the *Create project* green button at the bottom of the screen. You will see an *Importing in progress* message temporarily on your screen:

<img src="images/demo-system/17-proj-creation-in-progress.png" width="50%" height="50%">

Once the project is created, you will see the following:

<img src="images/demo-system/18-sample-proj-1-created.png" width="50%" height="50%">

Click [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/09.%20Releases.md#creating-a-project) to continue with this lab.
