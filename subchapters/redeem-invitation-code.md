## Redeeming your invitation code

> **NOTE:** Follow these instructions if you're using the GitLab Demo System. Otherwise, skip.

Your instructor will have provided you with an invitation code. Follow these steps to redeem your invitation:

1. Open your browser and enter the following URL in its address field:

> gitlabdemo.com

You will see the following screen:

<img src="images/demo-system/1-click-redeem-invitation.png" width="50%" height="50%">

2. Click on the *Redeem Invitation Code* blue button. In the following screen, enter the invitation code in field *Invitation Code*:

<img src="images/demo-system/2-click-create-account.png" width="30%" height="30%">

3. Click on the *Redeem and Create Account* blue button. The next screen will show your credentials to access the GitLab Demo System:

<img src="images/demo-system/3-your-credentials.png" width="40%" height="40%">

4. Click on the *Download Credentials* red button to save your credentials to your local drive.

> **NOTE:** Please keep this file in a safe place because you will need it throughout the workshop. Also, **keep the browser tab in step 1 above OPEN** because you will need to access your group by clicking on the *My Group* blue button throughout the workshop as well.

This completes the redemption of your invitation code.
