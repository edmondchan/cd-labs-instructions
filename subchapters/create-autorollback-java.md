## Creating a project called autorollback-java

In this lab, you will go through steps to create a project called *autorollback-java*.

1. **Creating a project**

Log on to your GitLab workspace. You should land on your *Projects* window:

> **NOTE:** if you don't see your projects, click on your *Projects* menu and then select *Your projects* to go to your **Projects** window.

<img src="images/rollback/38-go-to-workspace.png" width="75%" height="75%">

Click on the *New project* green button. On the **Create new project** window, click on the **Import project** panel:

<img src="images/rollback/39-click-import-proj.png" width="75%" height="75%">

On the **Import project from** screen click on the *Repo by URL* button:

<img src="images/rollback/40-click-on-repo-by-url.png" width="75%" height="75%">

This will expand the window and show fields to enter the URL of the repository you would like to import. In the field **Git repository URL**, enter the following:

> https://gitlab.com/tech-marketing/sandbox/autorollback-java.git

Also, ensure to select *Public* for the section **Visibility Level**. Your screen should look like this:

<img src="images/rollback/41-click-on-create-proj.png" width="75%" height="75%">

Click on the *Create project* green button at the bottom of the screen. You will see an *Importing in progress* message temporarily on your screen. Once the project is created, you will see the following:

<img src="images/rollback/42-proj-imported.png" width="75%" height="75%">

Click [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/07.%20Rollbacks.md#auto-rollback-on-alert-reading-lab-if-using-the-gitlab-demo-system) to continue with this lab.
