# Using GitLab with OpenShift

Due to the differences between OpenShift and Kubernetes, we are unable to provide integrated cluster creation capabilities for OpenShift within GitLab. We have been working on improving this, however. You can check the following [epic](https://gitlab.com/groups/gitlab-org/-/epics/2068) to read up on our work to make our integration to OpenShift better.

This section is not a lab with steps for you to follow. Rather, it is a reading lab that includes a set of useful resources of ways to integrate GitLab with OpenShift:

1. To be able to deploy from GitLab to OpenShift, you will first need an OpenShift cluster. You can install OpenShift on your laptop (you will need 32GB of RAM at least) by following these [instructions](https://gitlab.com/groups/gitlab-org/-/epics/2068#note_440112301).
2. Once you have your OpenShift cluster up and running, you may want to install a GitLab Runner to run as a pod in your OpenShift cluster. To do this, you can follow our [documentation](https://docs.gitlab.com/runner/install/openshift.html).
3. To build and deploy an application to OpenShift (running GitLab Runner) from GitLab, you will need to build a custom pipeline. You can find a sample pipeline that builds and deploys an application to OpenShift [here](https://gitlab.com/gitlab-org/gl-openshift/openshift-demos/openshift-custom-pipeline/-/blob/master/.gitlab-ci.yml).
4. This [video](https://youtu.be/5AbtSxpFQec) shows 2 and 3 above. Here's the [public project](https://gitlab.com/gitlab-org/gl-openshift/openshift-demos/openshift-custom-pipeline) used in the video.
5. This [blog post](https://www.openshift.com/blog/building-openshift-pipelines-with-gitlab) is another example of how to install GitLab Runner and deploy an application to an OpenShift cluster. Here's the [public project](https://github.com/mrjbanksy/container-pipelines/tree/gitlab-cicd-example/gitlab-cicd-example) used in this post, which includes a [sample pipeline](https://github.com/mrjbanksy/container-pipelines/blob/gitlab-cicd-example/gitlab-cicd-example/.gitlab-ci.yml).
6. If you would like to deploy applications running on OpenShift running on IBM Power, you can follow this [IBM developer recipe](https://developer.ibm.com/recipes/tutorials/deploying-a-multiarch-openshift-application-from-gitlab/).

## GitLab on OpenShift

You can install GitLab on Kubernetes with the cloud native GitLab Helm chart. Our [documentation](https://docs.gitlab.com/charts/installation/) provides instructions on how to do this for many Kubernetes distributions, such as EKS, GKE, AKS, etc.

Due to OpenShift’s increased security restrictions [we do not currently target or support it](https://docs.gitlab.com/charts/installation/cloud/openshift.html). However, we are working on fixing this. You can track our progress via this [epic](https://gitlab.com/groups/gitlab-org/-/epics/2068).


