## Creating a project called simple-java
In this lab, you will go through steps to create a project called *simple-java*.

1. **Creating a project**

Log on to your GitLab workspace. The first time you log in, you should see the following screen. Click on *Create a project*.

> **NOTE:** if this is not the first time you log in to GitLab workspace, head to your **Projects** window and click on the *New Project* blue button on the top right of your screen.

<img src="images/ado/ado-1-create-project.png" width="50%" height="50%">

On the next screen, click on *Import project*.

<img src="images/ado/ado-2-import-project.png" width="50%" height="50%">

On the next screen, click on *Repo by URL*.

<img src="images/ado/ado-3-import-from.png" width="75%" height="75%">

The screen will expand and show fields to populate for the import of a project via its URL.

<img src="images/ado/ado-4-enter-proj-info.png" width="50%" height="50%">

Enter the following project URL in the field **Git repository URL** field:

> https://gitlab.com/tech-marketing/sandbox/simple-java.git

and ensure that *Public* is checked for **Visibility Level**. Your screen should look similar to the following one:

<img src="images/ado/ado-5-enter-proj-URL-filled-out.png" width="50%" height="50%">

Next, click on the *Create project* button. You will see the following message for a few seconds while the project is being imported:

<img src="images/ado/ado-6-import-in-progress.png" width="50%" height="50%">

After a few seconds, the screen above will clear and you will be put into the newly created project:

<img src="images/ado/ado-7-proj-imported.png" width="50%" height="50%">

Click [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/01.%20Auto%20DevOps.md#creating-a-project-and-enabling-auto-devops) to continue with this lab.
