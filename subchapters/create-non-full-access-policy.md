## Creating a no-delete policy

> **IMPORTANT NOTE:** you need to do the instructions in this document in its entirety

This subsection creates a policy that will only give GitLab the permissions to be able to create resources, but not delete them.

Log on to the [AWS IAM Management Console](https://console.aws.amazon.com/iam/home) using your AWS credentials. Once logged on, in the navigation panel on the left, click on **Access management > Policies**:

<img src="images/aws/10-click-accessmgmt-policies.png" width="20%" height="20%">

On the next screen click on the *Create Policy* blue button:

<img src="images/aws/11-click-on-create-policy.png" width="75%" height="75%">

On the **Create Policy**, click on the *JSON* tab and paste the following text into it:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:CreateAutoScalingGroup",
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeScalingActivities",
                "autoscaling:UpdateAutoScalingGroup",
                "autoscaling:CreateLaunchConfiguration",
                "autoscaling:DescribeLaunchConfigurations",
                "cloudformation:CreateStack",
                "cloudformation:DescribeStacks",
                "ec2:AuthorizeSecurityGroupEgress",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:RevokeSecurityGroupEgress",
                "ec2:RevokeSecurityGroupIngress",
                "ec2:CreateSecurityGroup",
                "ec2:createTags",
                "ec2:DescribeImages",
                "ec2:DescribeKeyPairs",
                "ec2:DescribeRegions",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeSubnets",
                "ec2:DescribeVpcs",
                "eks:CreateCluster",
                "eks:DescribeCluster",
                "iam:AddRoleToInstanceProfile",
                "iam:AttachRolePolicy",
                "iam:CreateRole",
                "iam:CreateInstanceProfile",
                "iam:CreateServiceLinkedRole",
                "iam:GetRole",
                "iam:listAttachedRolePolicies",
                "iam:ListRoles",
                "iam:PassRole",
                "ssm:GetParameters"
            ],
            "Resource": "*"
        }
    ]
}
```

The text above will give GitLab the permissions to be able to create resources, but not delete them. Click on the *Next: Tags* blue button at the bottom to go to the **Add tags (Optional)** screen:

<img src="images/aws/13-add-tag-click-review.png" width="50%" height="50%">

Although optional, it's always useful to add a tag so that you can search for AWS resources you created. You can enter any key-value pair you want but I always like to enter the *gitlabuser* as the **Key** and my GitLab handle as the **Value**. In the screenshot above, you would need to replace the string **YOUR GITLAB ID** with your own GitLab handle. Click on the *Next: Review* blue button to move on to the **Review Policy** window:

<img src="images/aws/14-name-policy-click-create.png" width="75%" height="75%">

In the screenshot above, replace **YOUR-GITLAB-ID** with your own GitLab handle. I have chosen to add the suffix *-create-resources-policy* to the name of the policy, which describes what the policy is for. You can choose to name it anything you want, if you prefer. Click on the *Create policy* blue button at the bottom to create the policy.

## Creating the Provisioning Role ARN that uses the no-delete policy

Also known as the Provisioning Role ARN, this is the role that will authenticate to AWS and be the creator of the EKS cluster.

Go to the [IAM Console](https://console.aws.amazon.com/iam/) and log in using your AWS credentials. Once logged on, in the navigation panel on the left, click on **Access management > Roles**:

<img src="images/aws/15-click-accessmgmt-roles.png" width="20%" height="20%">

On the next screen, click on the *Create role* blue button:

<img src="images/aws/16-click-on-create-role.png" width="75%" height="75%">

On the **Create role** screen, click on **Another AWS account** tile to display the *Specify accounts that can use this role* fields. In the **Options**, click the *Require external ID (Best practice when a third party will assume this role)* checkbox to show the **External ID** input field. Now, enter the **Account ID** and **External ID** values from your Amazon EKS Cluster creation screen from GitLab (from [this screen](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/raw/master/images/auto-deploy/9-enter-your-role-ARN.png) from [this lab](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/create-eks-cluster.md)):

<img src="images/aws/22-enter-account-external-ids.png" width="50%" height="50%">

Click on the *Next: Permissions* blue button at the bottom of the screen. In the **Attach permissions policies** window, search for the name of the policy you created above in the first section of this lab. For example, I named mine *csaavedra-create-resources-policy* (yours will be different) so I entered *csaa* in the **Filter policies** field to find the policy I had created as the first step of this lab. Once the policy is found, click on its checkbox to select it:

<img src="images/aws/23-search-sel-policy-click-tags.png" width="75%" height="75%">

Click on the *Next: Tags* blue button at the bottom. You will see the following screen:

<img src="images/aws/24-add-tag-click-review.png" width="50%" height="50%">

Although optional, it's always useful to add a tag so that you can search for AWS resources you created. You can enter any key-value pair you want but I always like to enter the *gitlabuser* as the **Key** and my GitLab handle as the **Value**. In the screenshot above, you would need to replace the string **YOUR GITLAB ID** with your own GitLab handle. Click on the *Next: Review* blue button:

<img src="images/aws/25-name-role-arn-click-create.png" width="75%" height="75%">

In the screenshot above, replace **YOUR-GITLAB-ID** with your own GitLab handle. I have chosen to add the suffix *-provision-role-ARN* to the name of the role, which describes what the role is for. You can choose to name it anything you want, if you prefer. Click on the *Create role* blue button at the bottom to create the provision role ARN.

Once your provision role ARN is created, you will see the following message:

<img src="images/aws/26-role-arn-created-msg.png" width="50%" height="50%">

I named my provision role ARN *csaavedra-provision-role-ARN* (yours will be different). The name of your newly created provision role ARN in the message above is a link. Click on it to go to the role detail page. You will see a screen similar to the following one:

<img src="images/aws/27-role-arn-detail-redacted.png" width="50%" height="50%">

We have redacted portions of the fields in the screenshot above to cover specific account information. The field that you will need to copy to paste into the Create EKS screen on GitLab is the **Role ARN* above. Go ahead and copy it to your clipboard or to a file on your local computer now.

Now that you have created the no-delete policy and the provisioning role ARN, you can head back to the next step [Creating the service role](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/create-aws-roles-for-eks.md#creating-the-service-role).
