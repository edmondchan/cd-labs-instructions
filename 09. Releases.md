# Releases

Consider this scenario: Your team is working in the release of a re-branded web application, such initiative will require efforts from different stakeholders and multiple discussions throughout different issues created in distinct projects, to manage the portfolio of Projects and have a successful release, we will leverage *Epics, Milestones and issues*  


Before we start creating Issues, Iterations, Milestones and Epics, let's consider this diagram:  
## GitLab Agile Planning & Reporting Structure

<img src="images/release/MainDiagram.png" width="60%" height="60%">  

Having in mind the structure above, in this lab, we want to model an scenario that could be seen as:  

<img src="images/release/DiagramInit.png" width="60%" height="60%">  

Where:  
- **Issues**: The single unit of work, where an user story, idea, bug, etc. can be described
- **Iterations**: A way to track issues over a set period of time. Iterations can be used for 2x Weeks Sprints.
- **Milestones**: Another way to organize and track issues and Merge Requests over a set period of time, usually larger span than iterations and at a project level.
- **Epic**: Bucket of issues with a shared theme. Epics enable the management of a portfolio initiatives, capabilities, or features efficiently.

Considering the diagrams and concepts above, let's create our own Epic, Milestones and iterations which will result in a *Release*.

## Creating a project

1. Let's import a existing Java project to use in this lab. To create the project, do one of the following:

If you're using the GitLab Demo System, follow these [instructions](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/create-rel-sample-proj-demosys.md), **or**

If you're using GitLab.com or your own self-managed GitLab instance (with access to the internet), follow these [instructions](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/create-rel-sample-proj.md).

At this point, you should have a newly created project.

## Epics 

2. Given that *Epics* let you manage your portfolio of projects by tracking groups of issues that share a theme across projects and milestones, we will need to create at least another project to illustrate the concept. To do that, repeat the steps from 1 above (create another project within the group), with the exception that the second project should be named as *sample-project-2*.

3. Great, your group should now (at least) contain the two projects: *sample-project-1* and *sample-project-2*:

<img src="images/demo-system/23-group-view.png" width="50%" height="50%">   

4. In the group view is where we have the ability to manage Epics, click on *Epics* <img src="images/release/3.7ClickOnEpics.png" width="15%" height="15%">   

5. At this point, we don't have any *Epics* in the group, remember our scenario, we want to release a re-branded web application, let's then create an Epic where we will breakdown all the effort to create the  Front End part of application

<img src="images/release/3.9CreateEpicFront.png" width="75%" height="75%">   

Name this Epic *Front End Work*  and click on **Create Epic**

6. Recall we created two projects in previous steps. Those projects *sample-project-1 & sample-project2*  were already pre-populated with dummy issues, let's add some of those issues to our recently created *Front End Work* Epic. To do that, in the *Epic* view locate the *Add* button under *Epics And Issues* 


<img src="images/release/4.0AddIssueEpic.png" width="75%" height="75%">  

Make sure to add issues from both sample projects *1 & 2*, you can search the issues by number, in this case typing *#29* we can choose an issue from either project


<img src="images/release/4.2Add29.png" width="75%" height="75%">  

Let's add issues 4 issues in total, selecting a pair from each sample-project as shown below :  

<img src="images/release/4.3AddedIssues.png" width="75%" height="75%">   

7. Once you are done, your Epic could look like this: 

<img src="images/release/4.4ViewOfEpic.png" width="75%" height="75%">

8. Great, we have populated our *Epic* with issues from two different projects, in theory,  this issues share a common theme and will contain multiple discussions. We group them into a single view to gain an overview of the progress and development of a major feature.

## Milestones

9. Milestones help us to track and communicate a broader goal that shall be executed in a certain period of time. Milestones allow us to organize issues and merge requests into a cohesive group described with an optional start and finish date.  

Milestones can be used as:  
- Agile Sprints
- Releases  

In our case, we will create  a  *Milestone* to be used as a  *Release* for our rebranded web application 

10. Under the Epic view, click on *Issues > Milestones*  :

<img src="images/release/4.5Milestones.png" width="20%" height="20%">  

11. You will find that there are already some milestones created:  

<img src="images/release/4.6Milestones.png" width="75%" height="75%"> 

These milestones were automatically brought from the sample projects we created earlier using *Gitlab project template*

12. Let's select the first *Milestone* and edit it by renaming it, and adjusting the *start / due date*  to some time in the future that will make sense at the moment of reading this guide. Click on `Edit`:  

<img src="images/release/4.7MilestoneTimeAdjusting.png" width="75%" height="75%"> 

13. Rename the *Milestone* to *New Web UI ReleaseV1.0-RC1* and select a new *Start & Due* date :

<img src="images/release/4.8NewRelease.png" width="75%" height="75%">

14. After editing the *Milestone* click on **Save changes**. A view of the Milestone and its associated issues will open, as shown below:  

<img src="images/release/4.9MilestoneCreated2.png" width="75%" height="75%">

15. In the milestone view, hover or click  on *Issues* to expand the options menu and then click on **Iterations**  

16. You should see a message `No iterations to show`, let's change that and create a new one. Click on **New Iteration**  

<img src="images/release/5.2Iterations.png" width="75%" height="75%">

17. Name the new iteration `Sprint1-oak` and choose 1 week as its duration, such timeframe should be shorter and contained in the time-lapse previously stablished for the *Milestone* created in previous steps, in other words:  
*iteration_duration < Milestone_duration* In this case, We selected an iteration from Apr 1, 2021 - Apr 9, 2021 where our Milestone corresponds to a month from Apr 2021 until May 2021. 

<img src="images/release/5.3IterationCreated.png" width="75%" height="75%">

Great!, so far we have:
- Epic for Front End Work
- Milestone of one month corresponding to the launch of a first release
- Iteration that will take us closer to complete the work required for the first release

The steps followed so far seek to illustrate how to leverage these concepts in a simple use case where the release of a web application needs issue tracking and collaboration.

18. Lasty, let's take some of issues we previously added into the *Epic* and associate them with a *Milestone* and *Iteration*, to do that, being on the *iterations* page, click on *Issues* to bring up the list of these:

<img src="images/release/5.4GotoIssues.png" width="50%" height="50%">

19. Choose an issue from *sample-project-1* or the project you associated with a Milestone AND that belongs to the Epic. Remember *Milestones* are project level whereas *iterations* group level. In this example, we select the issue named `Nulla Tempore ...` from *sample-project-1*. On the right column of the issue page, it can be seen this issue belongs to an *Epic* and doesn't belong yet to any *Milestone* or *Iteration*

<img src="images/release/5.5IssueNullaTempore.png" width="20%" height="20%">  

20. Click on edit and associate the issue with the previously created Milestone and Iteration, it should look as the image below:   

<img src="images/release/5.6EditedIssueMilestone.png" width="20%" height="20%">

21. Now, if you go back to the group level view and select the *Epic* we created, you will see that it has a deadline now, and this one was inherited from our *Milestone*:  

<img src="images/release/5.7EpicInheritedMilestoneDates.png" width="75%" height="75%">

## Releases 

A GitLab Release is a snapshot of the source, build output, artifacts, and other metadata associated with a released version of your code. Let's create a release associating it with the project *Milestone* created earlier.

22. Go to *my-sample-project-1* (Or the project you associated the milestone with) and under *Project overview*, click on *Releases*  
<img src="images/release/5.8Release.png" width="50%" height="50%">  

click on **New release** <img src="images/release/5.9NewRelease.png" width="5%" height="5%">   

23. Fill in the details, make `Tag name = v1.0` and associate the release with `Milestone newUIv1.0-RC1` created earlier in this guide :  

<img src="images/release/6ReleaseDetails.png" width="80%" height="80%">   

Click on **Create release**  

24. Great!, now we can see the release has been created an associated with the completion of our **Milestone**, allowing us to keep track of its completion and therefore readiness of the project to get released.  

25. Once the released is created... 

<img src="images/release/6.1ReleaseAssociatedMilestone.png" width="80%" height="80%">

You can create a GitLab release on any branch. When you create a release:

- GitLab automatically archives source code and associates it with the release.
- GitLab automatically creates a JSON file that lists everything in the release, so you can compare and audit releases. This file is called [release evidence](https://docs.gitlab.com/ee/user/project/releases/#release-evidence).
- You can add release notes and a message for the tag associated with the release.

After you create a release, you can associate milestones with it, and attach release assets, like runbooks or packages.

When you create a release, if job artifacts are included in the last pipeline that ran, they are automatically included in the release as release evidence. Although job artifacts normally expire, artifacts included in release evidence do not expire. Check out our [documentation](https://docs.gitlab.com/ee/user/project/releases/#include-report-artifacts-as-release-evidence), for more information on including artifacts in the release evidence.

This streamlined process of release creation helps to reduce the release cycle times so that you can get your application faster to market.

## DORA metrics

The DevOps Research and Assessment ([DORA](https://www.devops-research.com/research.html)) team developed four key metrics as performance indicators for software development teams:

- Deployment frequency: How often an organization successfully releases to production.
- Lead time for changes: The amount of time it takes for code to reach production.
- Change failure rate: The percentage of deployments that cause a failure in production.
- Time to restore service: How long it takes an organization to recover from a failure in production.

These metrics provide a high-level systems view of an organization's software delivery and performance to measure the effectiveness of their development and delivery practices and predict their ability to achieve its goals.

As of this writing, GitLab has developed the project-level deployment frequency DORA metric. The rest of them will be [supported in the future](https://docs.gitlab.com/ee/user/analytics/ci_cd_analytics.html#supported-metrics-in-gitlab). For more information on this topic, check out [documentation](https://docs.gitlab.com/ee/user/analytics/ci_cd_analytics.html).

Up to the middle of lab **[07 Rollbacks](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/07.%20Rollbacks.md)**, you used a sample project called **Simple Java**. Head to this project as follows:

1. If you're using the GitLab Demo System, first head to your demo group by following these [instructions](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/open-your-demo-group.md). From your group, click on your previously created project **Simple Java**.

2. If you're using GitLab.com or your own self-managed GitLab instance (with access to the internet), login to your GitLab workspace, click on your Projects menu and select **Your projects**. Finally, click on your previously created project **Simple Java**.

Once you're in your project **Simple Java**, select **Analytics > CI / CD**:

<img src="images/release/7-click-analytics-cicd.png" width="30%" height="30%">

You will see the Pipelines related analytics:

<img src="images/release/8-cicd-analytics.png" width="50%" height="50%">

Depending on how many pipelines you ran on your project, your charts may look different from the ones shown above. The first chart shows the duration of the last 30 commits and the second chart shows the number of successful pipelines ran in the last week.

To see the DORA deployment frequency metric, click on the **Deployments* tab at the top of the window: 

<img src="images/release/9-dora-deploy-freq.png" width="50%" height="50%">

The chart above displays the frequency of deployments to the production environment, as part of the DORA 4 metrics. As the note above the chart mentions, the environment must be named **production** for its data to appear in the chart.

## Deploy Freezes

As organizations transition to Continuous Delivery practices, they may still need to set a Deploy Freeze window to temporarily halt automated deployments to production. This prevents unintended production releases during a period of time to help reduce uncertainty and risk of unscheduled outages.

Using the same project called **Simple Java** from the previous section, let's set up a deploy freeze.

Select **Settings > CI /CD** from the left menu of the project. On the Settings window, scroll down all the way to the bottom until you see the section for **Deploy freezes**. Click on its *Expand* button to reveal it:

<img src="images/release/10-deploy-freeze-expand.png" width="50%" height="50%">

The deploy freeze patterns follow the crontab syntax. You can use [crontab guru](https://crontab.guru/) as a scratchpad to build the dates and times you need. Let's say we need to prevent automated deployments to production from November 2 at 00 hours to November 15 at 23:59 hours. Click on the *Add deploy freeze* green button and enter the following values in the corresponding fields:

| Field name | Value 
| ----------- | ----------- |
| Freeze start | 0 0 2 11 *
| Freeze end | 59 23 15 11 *
| Cron time zone | [UTC -4] Eastern Time (US & Canada)

Your screen should look as follows:

<img src="images/release/11-add-deploy-freeze.png" width="30%" height="30%">

To finish creating the deploy freeze, click on the *Add deploy freeze* green button on the bottom right of the dialog window. Your **Deploy freezes** section should now look as follows:

<img src="images/release/12-deploy-freeze-created.png" width="50%" height="50%">

This concludes this lab. Now, let's move on to the topic of Auto Deploy.
